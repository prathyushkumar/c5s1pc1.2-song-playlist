package songplaylist;
import java.util.LinkedList;
public class SongsPlaylist {
	LinkedList<Song> list = new LinkedList<Song>();
	
	void addSongAtBeggining(String songName,String songDuration)
	{
		list.addFirst(new Song(songName,songDuration));
		System.out.println("Successfully added the Song "+songName+" into the Playlist");
	}
	
	void displaySizeOfPlaylist()
	{
		System.out.println("Size of the Playlist is"+" "+list.size());
	}
	
	void addSongAtPosition(int position,String songName,String songDuration)
	{
		list.add(position, new Song(songName,songDuration));
		System.out.println("Successfully added the Song "+songName+" into the Playlist at the position "+position);
	}
	
	void deleteSong(String songName)
	{
		for(Song i:list)
		{
			if(i.getSongName().equals(songName))
			{
				list.remove(i);
				System.out.println("Successfully deleted "+i.getSongName());
			}
		}
		
	}
	public static void main(String[] args) {
		SongsPlaylist sp = new SongsPlaylist();
		sp.addSongAtBeggining("hello", "4:30");
		sp.addSongAtBeggining("hello1", "4:30");
		sp.addSongAtBeggining("hello2", "4:30");
		sp.deleteSong("hello");
		sp.addSongAtPosition(1, "why", "3:40");
		sp.displaySizeOfPlaylist();
	}
}
